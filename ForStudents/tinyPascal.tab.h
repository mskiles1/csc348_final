/* A Bison parser, made by GNU Bison 2.3.  */

/* Skeleton interface for Bison's Yacc-like parsers in C

   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     END_ = 258,
     BEGIN_ = 259,
     VAR = 260,
     PROGRAM = 261,
     WRITELN = 262,
     WRITE = 263,
     UNTIL = 264,
     REPEAT = 265,
     WHILE = 266,
     DO = 267,
     DOWNTO = 268,
     TO = 269,
     FOR = 270,
     THEN = 271,
     IF = 272,
     ELSE = 273,
     PERIOD = 274,
     COLON = 275,
     SEMICOLON = 276,
     COMMA = 277,
     NOT_EQUAL = 278,
     EQUAL = 279,
     GREATER_EQUAL = 280,
     GREATER = 281,
     LESSER_EQUAL = 282,
     LESSER = 283,
     OR = 284,
     MINUS = 285,
     PLUS = 286,
     AND = 287,
     REAL_DIV = 288,
     INT_DIV = 289,
     STAR = 290,
     NOT = 291,
     ASSIGN = 292,
     END_PAREN = 293,
     BEGIN_PAREN = 294,
     IDENTIFIER = 295,
     SIMPLE_TYPE = 296,
     BOOLEAN = 297,
     INTEGER = 298,
     REAL = 299,
     STRING = 300
   };
#endif
/* Tokens.  */
#define END_ 258
#define BEGIN_ 259
#define VAR 260
#define PROGRAM 261
#define WRITELN 262
#define WRITE 263
#define UNTIL 264
#define REPEAT 265
#define WHILE 266
#define DO 267
#define DOWNTO 268
#define TO 269
#define FOR 270
#define THEN 271
#define IF 272
#define ELSE 273
#define PERIOD 274
#define COLON 275
#define SEMICOLON 276
#define COMMA 277
#define NOT_EQUAL 278
#define EQUAL 279
#define GREATER_EQUAL 280
#define GREATER 281
#define LESSER_EQUAL 282
#define LESSER 283
#define OR 284
#define MINUS 285
#define PLUS 286
#define AND 287
#define REAL_DIV 288
#define INT_DIV 289
#define STAR 290
#define NOT 291
#define ASSIGN 292
#define END_PAREN 293
#define BEGIN_PAREN 294
#define IDENTIFIER 295
#define SIMPLE_TYPE 296
#define BOOLEAN 297
#define INTEGER 298
#define REAL 299
#define STRING 300




#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
#line 33 "tinyPascal.y"
{
  std::list<std::string*>*	identifierPtrListPtr_;
  Node*				nodePtr_;
  StatementListNode*		statementListNodePtr_;
  std::string*			strPtr_;
  bool				bool_;
  int				integer_;
  float				real_;
  char*				charPtr_;
  simpleType_ty			simpleType_;
}
/* Line 1529 of yacc.c.  */
#line 151 "tinyPascal.tab.h"
	YYSTYPE;
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif

extern YYSTYPE yylval;

