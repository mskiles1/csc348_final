cmake_minimum_required(VERSION 3.8)
project(ForStudents)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES
        LabelMention.h
        Node.h
        SymbolTable.h
        tinyPascal.cpp
        tinyPascal.h
        tinyPascal.lex
        tinyPascal.y
        VariableMention.h)

add_executable(ForStudents ${SOURCE_FILES})